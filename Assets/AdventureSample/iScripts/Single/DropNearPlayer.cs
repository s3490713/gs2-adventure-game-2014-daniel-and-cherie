using UnityEngine;
using System.Collections;

public class DropNearPlayer : SingleIScript
{
	protected override void OnSuccess()
	{
		Vector3 playerPos = Helpers.Player.transform.position;
		Vector3 point = new Vector3(
			playerPos.x,
			playerPos.y + 1.8f,
			playerPos.z);
		
		Helpers.Inventory.RemoveInteractable(
			Interactable, point);
	}
	
	protected override void OnOutOfRange()
	{
	}
}

