# Game Studio 2 - Adventure Game Base Project #
Written for RMIT GS2 by Stephen Karpinskyj
Modified by Ben Wallis, Cherie Davidson and Daniel Kidney

## Contact Information ##
**Offering Lecturer** - Gerry Sakkas
[gerry.sakkas@rmit.edu.au](mailto:gerry.sakkas@rmit.edu.au)

**Head Tutor** - Cherie Davidson
[cherie.davison@rmit.edu.au](mailto:cherie.davidson@rmit.edu.au)

**Tutor** - Daniel Kidney
[daniel.kidney@rmit.edu.au](mailto:daniel.kidney@rmit.edu.au)